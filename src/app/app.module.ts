import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {HttpClientModule} from "@angular/common/http";

import {AccountModule} from "./account/account.module";
import {AppComponent} from "./app.component";
import {SharedModule} from "./shared/shared.module";
import {appRoutes} from "./app.routes";
import {BsDropdownModule, ModalModule} from "ngx-bootstrap";
import {HomeModule} from "./home/home.module";

@NgModule({
  imports: [
    BrowserModule,
    SharedModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    AccountModule,
    ModalModule.forRoot(),
    BsDropdownModule.forRoot(),
    HomeModule
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent]
})

export class AppModule {
}

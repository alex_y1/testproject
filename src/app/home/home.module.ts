import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from "@angular/common/http";

import {homeRoutes} from "./home.routes";
import {UtilsService} from "../shared/utils.service";
import {HomeComponent} from "./home.component";
import {UserService} from "./user.service";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forChild(homeRoutes),
  ],
  providers: [UtilsService, UserService],
  declarations: [HomeComponent]
})

export class HomeModule {
}

import {Injectable} from '@angular/core';

@Injectable()
export class User {
  name: string;
  description: string;
  email: string;

  constructor() {

  }

}

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

import {NotificationService} from "../shared/notifications/notification.service";

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss'],
})

export class HomeComponent implements OnInit {

    constructor(private notificationService: NotificationService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
    }

}

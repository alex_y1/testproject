import {Routes} from '@angular/router';

import {UserRouteAccessService} from '../shared/user-route-access.service';
import {HomeComponent} from './home.component';

export const homeRoutes: Routes = [
    {
        path: 'home',
        data: {breadcrumbs: "Home"},
        canActivate: [UserRouteAccessService],
        children: [
            {
                path: '',
                component: HomeComponent,
            }
        ]
    }
];

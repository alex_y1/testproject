import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {User} from "./user.model";

@Injectable()
export class UserService {
  backendUrl = 'http://localhost:8080';

  constructor(private http: HttpClient) {
  }

  // getById(id: string) {
  //   return this.http.get<User>(this.baseUrl + '/' + id);
  // }

  update(user: User) {
    console.log("user", user);
    return this.http.post(this.backendUrl + '/update', user);
  }

  // remove(entity: T) {
  //   return this.http.delete(this.baseUrl + '/' + entity._id);
  // }
}

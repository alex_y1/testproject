import {Routes} from '@angular/router';

import {HomeComponent} from "./home/home.component";

export const appRoutes: Routes = [
    {path: '', redirectTo: '/home', pathMatch: 'full'},
    {path: '**', component: HomeComponent}
];

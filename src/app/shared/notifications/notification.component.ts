import {Component} from '@angular/core';

import {NotificationService} from './notification.service';
import {NotificationElement} from './notification.model';

@Component({
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.scss']
})

export class NotificationComponent {
    notifications: NotificationElement[] = [];

    constructor(private notificationService: NotificationService) {
    }

    ngOnInit() {
        this.notificationService.getNotificationObservable()
            .subscribe((notification: NotificationElement) => {
                if (notification.visible) {
                    this.removeNotification(notification);
                } else {
                    this.notifications.push(notification);
                    notification.visible = true;
                }
            });
    }

    removeNotification(notification: NotificationElement) {
        let index = this.notifications.indexOf(notification);
        if (index >= 0) {
            this.notifications.splice(index, 1);
        }
    }

}

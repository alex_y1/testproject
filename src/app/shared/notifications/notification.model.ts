import {Injectable} from '@angular/core';

@Injectable()
export class NotificationElement {
    type: string;
    message: string;
    timeout: number;
    timestamp: number;
    visible: boolean;

    constructor(type: string, message: string, timeout: number) {
        this.type = type;
        this.message = message;
        this.timeout = timeout;
        this.timestamp = new Date().getTime();
        this.visible = false;
    }
}

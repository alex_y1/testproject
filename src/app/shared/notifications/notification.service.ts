import {Injectable} from '@angular/core';

import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {NotificationElement} from './notification.model';

@Injectable()
export class NotificationService {
  private notificationSubject = new Subject<NotificationElement>();

  getNotificationObservable(): Observable<NotificationElement> {
    return this.notificationSubject.asObservable();
  }

  success(message: string, obj?: any) {
    console.log(message, obj);
    this.alert("success", message, 3000);
  }

  warning(message: string, obj?: any) {
    console.log(message, obj);
    this.alert("warning", message, 3000);
  }

  error(message: string, obj?: any) {
    console.error(message, obj);
    this.alert("danger", message, 3000);
  }

  alert(type: string, message: string, timeout: number) {
    let notification = new NotificationElement(type, message, timeout);
    this.notificationSubject.next(notification);
    setTimeout(() => {
      this.notificationSubject.next(notification);
    }, timeout);
  }

}

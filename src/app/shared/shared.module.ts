import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule} from "@angular/router";

import {AlertModule} from "ngx-bootstrap";
import {NotificationComponent} from "./notifications/notification.component";
import {UserRouteAccessService} from "./user-route-access.service";
import {AuthService} from "./auth.service";
import {NotificationService} from "./notifications/notification.service";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    AlertModule
  ],
  declarations: [
    NotificationComponent
  ],
  exports: [
    NotificationComponent
  ],
  providers: [
    UserRouteAccessService,
    AuthService,
    NotificationService
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})

export class SharedModule {
}

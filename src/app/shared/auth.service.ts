import {Injectable} from "@angular/core";
import {Router} from "@angular/router";
import {NotificationService} from "./notifications/notification.service";

@Injectable()
export class AuthService {

  constructor(private router: Router,
              private notificationService: NotificationService) {
  }

  isLoggedIn(): boolean {
    // return !!(this.getSessionToken() && this.getJwtToken());
    return true;
  }

  //
  // getUsername() {
  //     return this.localStorage.get(this.sessionUserName);
  // }
  //
  // login(username: string, password: string): Promise<any> {
  //   return new Promise((resolve, reject) => {
  //     if (username === "admin" && password === '1234') {
  //       resolve();
  //
  //     } else {
  //       reject("Error");
  //     }
  //   });
  // }

  goLogout(forceGo: boolean) {
    if (forceGo) {
      window.location.assign("/login");
    } else {
      this.router.navigate(['/login'])
        .catch((error) => this.notificationService.error("Navigation error.", error));
    }
  }

  logoutMain() {
    // this.authClient.signOut()
    //     .then(() => {
    //         this.notificationService.log('Successfully logged out.');
    //     })
    //     .fail((err: any) => {
    //         this.notificationService.errorLog('Error occurred while logging out.', err);
    //     });
  }

  logout(forceGo?: boolean) {
    this.logoutMain();
    this.goLogout(forceGo);
  }

}

import {Injectable} from '@angular/core';
import {FormGroup} from '@angular/forms';

@Injectable()
export class UtilsService {

    static getValidationClass(form: FormGroup, field: string) {
        let control = form.get(field);
        if (control.dirty || control.touched) {
            if (control.status === 'INVALID') {
                return 'has-error';
            } else if (control.status === 'VALID') {
                return 'has-success';
            }
        }
        return '';
    }
}

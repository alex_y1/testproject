import {CanActivate} from '@angular/router';
import {Injectable} from '@angular/core';

import {AuthService} from "./auth.service";

@Injectable()
export class UserRouteAccessService implements CanActivate {

    constructor(private authService: AuthService) {
    }

    canActivate(): boolean {
        if (this.authService.isLoggedIn()) {
            return true;
        } else {
            this.authService.logout();
            return false;
        }
    }

}

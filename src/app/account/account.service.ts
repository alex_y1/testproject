import {Injectable} from "@angular/core";
import {FormBuilder, Validators} from "@angular/forms";

@Injectable()
export class AccountService {

    constructor(private fb: FormBuilder) {}

    createForm() {
        return this.fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
}
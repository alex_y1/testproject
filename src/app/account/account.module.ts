import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';

import {accountRoutes} from './account.routes';
import {LoginComponent} from './login/login.component';
import {AccountService} from "./account.service";
import {UserService} from "../home/user.service";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild(accountRoutes)
    ],
    declarations: [
        LoginComponent,
    ],
    providers: [AccountService, UserService]
})

export class AccountModule {
}

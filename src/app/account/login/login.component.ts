import {FormGroup} from "@angular/forms";
import {Component, OnInit} from "@angular/core";
import {Router} from "@angular/router";

import {UtilsService} from "../../shared/utils.service";
import {NotificationService} from "../../shared/notifications/notification.service";
import {AccountService} from "../account.service";
import {AuthService} from "../../shared/auth.service";
import {UserService} from "../../home/user.service";
import {User} from "../../home/user.model";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  getValidationClass = UtilsService.getValidationClass;
  user: User;

  constructor(private authService: AuthService,
              private router: Router,
              private notificationService: NotificationService,
              private accountService: AccountService,
              private userService: UserService) {
  }

  ngOnInit() {
    // if (this.authService.isLoggedIn()) {
    //     this.goHome();
    // }
    this.loginForm = this.accountService.createForm();
  }

  login(value: any) {
    this.user = new User();
    this.user.email = value.username;
    this.userService.update(this.user).subscribe(resp =>  {
      console.log("resp", resp);
    });
    this.notificationService.success("Login is successful!");
    this.goHome();
  }

  goHome() {
    this.router.navigate([''])
      .catch((error) => this.notificationService.error("Navigation error.", error));
  }

}
